/**
 * Created by 03004803 on 10/09/2014.
 */

var map;
var MY_MAPTYPE_ID = 'custom_style';
var startPosition = new google.maps.LatLng(55.9331782,-3.2129843);

var bins = [];
var binLocations = [];
var player = new google.maps.Circle( {} );
//var player = new Player();

function main_init()
{

    var featureOpts = [
        {
            stylers: [
                { hue: '#123456' },
                { visibility: 'simplified' },
                { gamma: 0.5 },
                { weight: 0.5 }
            ]
        },
        {
            elementType: 'labels',
            stylers: [
                { visibility: 'on' }
            ]
        },
        {
            featureType: 'water',
            stylers: [
                { color: '#DC2300' }
            ]
        }
    ];



    var mapOptions = {
        zoom: 18,
        center: startPosition,
        disableDoubleClickZoom: true,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.SATELLITE, MY_MAPTYPE_ID]
        },
        mapTypeId: MY_MAPTYPE_ID,
        scrollwheel: false
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var styledMapOptions = {
        name: 'Custom Style'
    };

    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

    var testLoc = new google.maps.LatLng(55.9331782,-3.2129843);

   // var testMarker = createPaperBinMarker(testLoc);

    var circleOptions =
    {
        strokeColor: '#00FF00',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#00FF00',
        fillOpacity: 0.65,
        map: map,
        center: startPosition,
        radius: undiscoveredRadius
    };
   // var circle = new google.maps.Circle(circleOptions);

   /* var playerOptions =
    {
        strokeColor: '#0000FF',
        strokeWeight: 5,
        fillColor: '#00FFFF',
        fillOpacity: 0.95,
        map: map,
        center: startPosition,
        radius: 10
    };
*/
    binLocations.push(new google.maps.LatLng(55.935412, -3.219821));
    binLocations.push(new google.maps.LatLng(55.931614, -3.217310));
    binLocations.push(new google.maps.LatLng(55.935719, -3.213705));
    binLocations.push(new google.maps.LatLng(55.934066, -3.210315));



    for (var i = 0; i < 4; ++i) {
        var rand = Math.floor(Math.random() * 3);
        var type;
        var val;
        switch (rand)
        {
            case 0:
                type = 'paper';
                val = 15;
                break;
            case 1:
                type = 'grey';
                val = 25;
                break;
            case 2:
                type = 'brown';
                val = 35;
                break;
            default:
                alert ("oh noes!");
        }
        var loc = binLocations[i];


        bins.push(new Bin(type, val, loc));
    }

    player.setOptions({
        strokeColor: '#0000FF',
        strokeWeight: 5,
        fillColor: '#00FFFF',
        fillOpacity: 0.95,
        map: map,
        center: startPosition,
        radius: 10
    });



    google.maps.event.addListener(map, 'dblclick', function(event)
    {
        var latLng = event.latLng;
       // alert(latLng.toString());
        player.setCenter(latLng);
        map.panTo(latLng);

        for (var i = 0; i < bins.length; ++i)
        {
            if (!bins[i].isDiscovered())
                bins[i].inRange(player);
        }

    });

}
