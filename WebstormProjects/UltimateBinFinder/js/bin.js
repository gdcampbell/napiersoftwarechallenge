/**
 * Created by 03004803 on 09/09/2014.
 */

var brownBinImage = 'Icons/brown_bin.png';
var paperBinImage = 'Icons/paper_bin.png';
var greyBinImage = 'Icons/grey_bin.png';

var Bin = function()
{
    this.constructor.apply(this, arguments);
}
var undiscoveredRadius = 35;
var discoveredRadius = 55;
Bin.prototype.constructor = function(type, value, location) {
    this.type = type;
    this.value = value;
    this.location = location;
    this.discovered = false;


    switch (this.type)
    {
        case 'paper':
            this.color = '#0099A1';
            this.marker = createBinMarker(this.location, paperBinImage);
            break;
        case 'grey':
            this.color = '#222222';
            this.marker = createBinMarker(this.location, greyBinImage);
            break;
        case 'brown':
            this.color = '#790909';
            this.marker = createBinMarker(this.location, brownBinImage);
            break;
    }
    this.marker.setClickable(false);
    this.color = (type === 'paper') ? '#0099A1':(type === 'grey')?'#222222':'#790909';

    this.circle = drawMarkerCircle(this.location, this.color, undiscoveredRadius);
}



Bin.prototype.getValue = function(){

    return this.value;
}

Bin.prototype.getType = function()
{
    return this.type;
}

Bin.prototype.getLocation = function()
{
    return this.location;
}

Bin.prototype.inRange = function(player)
{
    var pLoc = player.getCenter();
    var dist = google.maps.geometry.spherical.computeDistanceBetween(pLoc, this.location);
    //alert (dist);
    if (dist <= undiscoveredRadius)
    {
        this.discovered = true;
        this.marker.setVisible(false);
        this.circle.setOptions(
            {
                fillColor: '#FFFFFF',
                fillOpacity: 0.25,
                map: map,
                center: this.location,
                radius: discoveredRadius
            }

        );
    }

}

Bin.prototype.isDiscovered = function()
{
    return this.discovered;
}




function createBinMarker(location, icon)
{
    return new google.maps.Marker(
        {
            position: location,
            map: map,
            icon: icon,
            draggable: false


        }
    );
}

function drawMarkerCircle(location, color, radius)
{
    return new google.maps.Circle(
        {
            strokeColor: color,
            strokeWeight: 2.5,
            fillColor: color,
            fillOpacity: 0.85,
            map: map,
            center: location,
            radius: radius,
            clickable: false
        }
    );
}