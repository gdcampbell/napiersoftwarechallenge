/**
 * Created by 03004803 on 11/09/2014.
 */

var Player = function()
{
    this.constructor.apply(this, arguments);
}

Player.prototype.constructor()
{
    this.position = new google.maps.LatLng(55.9331782,-3.2129843);
    this.circle = new google.maps.Circle(
        {
            strokeColor: '#0000FF',
            strokeWeight: 5,
            fillColor: '#00FFFF',
            fillOpacity: 0.95,
            map: map,
            center: startPosition,
            radius: 10
        });
    Math.floor(Math.random());
}

Player.prototype.getPosition = function()
{
    return this.position;
}

Player.prototype.setPosition = function(newLocation)
{
    this.position = newLocation;
    this.circle.setCenter(this.position);
}